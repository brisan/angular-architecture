var app = angular.module('calendar', ['ui.router', 'angular-growl', 'pascalprecht.translate', 'ui.select']);


//Definimos templates
var templates = {}
for(var t in impactaTemplates){
	templates[t] = decodeURIComponent(impactaTemplates[t]);
}
app.value('templates', templates);


//Definimos routers
var config = function($stateProvider, $urlRouterProvider, growlProvider, $translateProvider) {

	$translateProvider
		.useStaticFilesLoader({
			prefix: '/locales/',
			suffix: '.json'
		})
		.registerAvailableLanguageKeys(['en', 'es'])
		.preferredLanguage('en')
		.useSanitizeValueStrategy('escapeParameters');

	$stateProvider.state({
		name: 'calendar',
		url: '/',
		views : {
			'' : {
				template : templates['calendar'],
				controller : 'calendarCtrl'
			}
		}
	});

	$urlRouterProvider.otherwise('/');
	growlProvider.globalTimeToLive(5000);
}
config.$inject = ['$stateProvider', '$urlRouterProvider', 'growlProvider', '$translateProvider'];
app.config(config);
